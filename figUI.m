% FIGUI is a class which streamlines the process of writing interactive 
%    figures for exploratory data analysis. FIGUI also provides a scripting
%    engine to emulate user interaction, which enables users to write 
%    scripts to automate commonly performed actions or perform an analysis,
%    developed interactively, in a repeatable fashion. FIGUI is intended to
%    be used in conjunction with a calling .m file (such as the example 
%    below) which implements the application-specific figure control and
%    any custom callbacks. 
%
%  Author:         Calvin Eiber
%  e-mail address: calvin.eiber@sydney.edu.au
%  Release:        1.0
%  Release date:   24 November 2015
%
%  Example Project: 
%   http://www.mathworks.com/matlabcentral/fileexchange/54142-tableexplorer
% 
% Methods - Initial Setup:
% ui = ui.bind(ui, cmd, callback)
% ui = ui.bindDefaults(ui, [fileExt], [initFcn], [helpFcn])
% ui = ui.loop(ui)
%
% Methods - Interactive input and scripting:
% [ui, str] = ui.input([prompt]);
% [ui,(x,y,b)] = ui.ginput;
% [x,y,b] = ui.click;
% [list] = ui.getBindings;
% [token] = ui.getToken;
%
% Methods - Built-in Callbacks (and default key assignment):
% 'L': ui.load(ui,[fileSpec])
% ' ': ui.pauseSuspend(ui) (spacebar)
% '!': ui.parseScript(ui,[scriptName])
% '+': ui.import(ui)
% '=': ui.export(ui)
% 'Q': ui.stop(ui)        
%  1 : ui.parseClick(ui)
% 'O': ui.editState(ui)
% 
% Other hard-coded key assignments:
% '$': returns to the base workspace to execute one command. 
% 'Esc' key: causes the UI to exit from its main loop.
% 
%  Scripting Syntax: 
%          Commands in a script are converted into a cell array, which are
%          then executed as though the user had supplied the program with
%          that input, either through ui.ginput or ui.input. If you do not
%          want a feature to be script-capable, do not use ui.ginput or
%          ui.input (use ginput and input instead). In addition to the
%          syntax specified by the callbacks in the calling .m file, 
%          scripts have the following additional syntax: 
% 
%  $~ evaluate expression at run-time in the base workspace.
%  $  evaluate expression at run-time in the base workspace; return the
%     result as the next token in the script
%  @~ evaluate expression at compile-time in the base workspace.
%  @  evaluate expression at compile-time in the base workspace; return the 
%     result as a token in the script
%  @{ (condition) loop over commands in script at compile-time, using a 
%  @} (iterator)  while-loop-style syntax. Example:
%  
%  @~ config_var_values = [1, 2, 5, 10, 20];
%  @~ ii = 1;
%  @~ UI_export = {};
%  @{ ii < length(config_var_values)    % While condition
%     o 'config_var_name'               % Edit state var in loop
%     @  config_var_values(ii)          % Value to set state var to
%     '='                               % ui.export
%     $~ Analysis_result{end+1} = UI_state
%  @} ii = ii+1;                        % increment
%  $~ save UI_analysis.mat Analysis_result
% 
%  NOTE: Nested loops are theoretically supported but not fully tested.
% 
%          Literal tokens: 
%  [1,2,3] % array literal
%  {1,2,3} % cell array literal
%  45      % scalar literal
%  39 29   % two tokens, {39} {25}
%  string_literal
% "string literal"
% 'string literal'
%  string literal % two tokens, {'string'} {'literal'}
%
% NOTE: array and cell array literals are constructed at compile-time via a
%       call to evalin(base) and so can be used to pass information from
%       the base workspace to the UI. For example:
%
% @~ load physical_constants.mat % contains c, G, h, h_c, etc.
% o 'speed_of_light' [c] % edit state var
% 
%  ui.ginput interprets a length(1) token as a keystroke at x,y = 0
%                       a length(2) token as a left mouse click at x,y
%                       a length(3)+ token as a keystroke at x,y, using the
%                       format [x y b] It is legal to mix characters and
%                       numbers e.g. [1.23 3.75 'x']



classdef figUI
    
    properties  
%       Structure to hold loaded data or config variables which the user
%       should not be able to access directly.
        data    = struct;       
%       Structure to hold config variables which the user can edit interactively, either by name (using ui.editState) or by helper functions (supplied by the calling .m file)
        state   = struct;
%       Array of graphics objects (2014b+) associated with the interface. Typically a figure window.
        
        % handles = gobjects(0); 
        handles = []; 
%       Function handle of the form function ui = drawFcn(ui) which is called each time a callback is executed.
        redraw = @(s) s;        
%       Default string prompt associated with ui.input.
        prompt = 'ui>> ';

    end
    
    properties (Hidden)        
        
        script = {};
        cmd = [];
        callback = {};
        initializer = @(s) s;

        textInputMode = false;
        printTarget = [];
        stopFlag = false;
        clickDat = [0,0,0];
    end
    
    methods
        
        % Initial Setup
        
        function ui = bind(ui, n_cmd, n_callback)
%       ui.bind(ui, cmd, callback)
%         .bind associates a callback of the form ui = callback(ui) with
%               the keystroke 'cmd'. 'cmd' may be either a character
%              (case-insensitive) or a number associated with a
%               non-printing character, as would be returned by 
%               [~,~,b] = ginpout. Common numeric command keys:
%                    1 = Mouse left button
%                    2 = Mouse middle button
%                    3 = Mouse right button
%                    8 = Backspace key
%                    9 = Tab key
%                   10 = Enter key (this is modified behaviour from ginput)
%                   27 = Esc key (reserved; terminates program)
%                   28 = Left arrow key
%                   29 = Right arrow key
%                   30 = Up arrow key
%                   31 = Down arrow key
%                  127 = Del key
            
            ui.cmd(end+1) = lower(n_cmd);
            ui.callback{end+1} = n_callback;            
        end
        
        function ui = bindDefaults(ui, fileExt, initFcn, helpFcn)
%       ui.bindDefaults(ui, [fileExt], [initFcn], [helpFcn])
%         .bindDefaults is equivalent to 
%             ui = ui.bind('!', @(s) parseScript(s) );
%             ui = ui.bind('+', @(s) import(s) );
%             ui = ui.bind('=', @(s) export(s) );
%             ui = ui.bind('O', @(s) editState(s) );
%             ui = ui.bind('Q', @(s) stop(s) );
%             ui = ui.bind(' ', @(s) pauseSuspend(s) );
%             ui = ui.bind( 1 , @(s) parseClick(s) );
%           if helpFun was supplied, 
%             ui = ui.bind('H', @(s) helpFcn(s) );
%           if fileExt was supplied,
%             ui = ui.bind('L', @(s) load(s, file_ext) );
%           if initFcn was supplied, the supplied initFcn is called at the
%             end of ui.load to set up application-specific configuration 
%             variables or perform any data pre-processing. 
           
            if exist('helpFcn','var'), ui = ui.bind('H', helpFcn); end
            if exist('initFcn','var'), ui.initializer = initFcn;   end                        
            if exist('fileExt','var'), 
                ui = ui.bind('L', @(s) load(s, fileExt) ); 
            end
            
            ui = ui.bind('!', @(s) parseScript(s) );
            ui = ui.bind('+', @(s) import(s) );
            ui = ui.bind('=', @(s) export(s) );
            ui = ui.bind('O', @(s) editState(s) );
            ui = ui.bind('Q', @(s) stop(s) );
            ui = ui.bind(' ', @(s) pauseSuspend(s) );
            ui = ui.bind( 1 , @(s) parseClick(s) );
            
        end
                             
        function ui = loop(ui)
%       ui.loop(ui)
%         .loop causes the ui to enter its main loop, in which it waits for
%          input from the user (if the calling .m file has added anything
%          to ui.handles, input is via ui.ginput; otherwise, input is via
%          the command line through ui.input) and executes user input or
%          scripted commands until ui.stop is called.

            % Error-check (for Matlab 2014b+)
            if ~all(ishandle(ui.handles)) 
                warning('figUI: invalid or deleted figure handle.')
                ui.handles = figure('Color','w');            
            end
            
            ui.stopFlag = false;
            while ~ui.stopFlag
                
                ui = ui.redraw(ui);
                
                if isempty(ui.handles), ui.textInputMode = true; end 
                                
                if ui.textInputMode, 
                    [ui, b] = ui.input();
                else                    
                    if strcmpi(get(ui.handles(1),'Type'), 'figure')
                        figure(ui.handles(1))
                    elseif strcmpi(get(ui.handles(1),'Type'), 'axes')
                        axes(ui.handles(1)) %#ok<LAXES>
                    end                    
                    ui = ui.ginput();
                    [~,~,b] = ui.click();
                end
                
                if b(1) == '$' % execute in base
                    
                    if isempty(ui.script), 
                        evalin('base',input('>> ','s'));                    
                    elseif strncmpi(b,'$~',2)
                        evalin('base',ui.script{1});
                        ui.script = ui.script(2:end);
                    else
                        try
                            ui.script = [evalin('base',ui.script{1}) ui.script(2:end)];
                        catch                            
                            evalin('base',ui.script{1})
                        end
                    end
                
                elseif b(1) == 27, ui.stopFlag = true;
                else
                    i = find(ui.cmd == b(1), 1, 'last');
                    if ~isempty(i)
                        try
                            ui = ui.callback{i}(ui); 
                        catch C
                            % warning(C.getReport);
                            warning(C.message); 
                        end
                    end
                end
                
            end
            
        end
        
        % Builtin Callbacks
        
        function ui = load(ui, fileSpec)
% 'L':  ui.load(ui,[fileSpec])
%          If fileSpec exists as a file, ui.data = load(fileSpec).
%          Otherwise, the user is prompted to select a file matching
%          fileSpec (default: '*.mat') using the default file selction
%          dialog. 


            if ~exist('fileSpec','var'), fileSpec = '*.mat'; end
            
            if ~isempty(ui.script) && exist(ui.script{1}, 'file')
                fileSpec = ui.script{1};
                ui.script = ui.script(2:end);                
            end
            
            if fileSpec == 0, return, end
                        
            if exist(fileSpec,'file')                
                [fileSpec, filepath] = strtok(fliplr(fileSpec),'\');
                fileSpec = fliplr(fileSpec);
                filepath = fliplr(filepath);               
            else                                                
                if ~isfield(ui.state, 'filepath'), 
                    [fileSpec, filepath] = uigetfile(fileSpec);
                else
                    [fileSpec, filepath] = uigetfile(fileSpec,[],...
                                                     ui.state.filepath);
                end
            end           
            
            if fileSpec == 0, return, end
            
            disp(['Loading ' fileSpec])            
            ui.data = load([filepath fileSpec]);
                        
            ui.state.filename = fileSpec;
            ui.state.filepath = filepath;
            ui.state.update = true;
            
            ui = ui.initializer(ui);
        end
        
        function ui = parseScript(ui, scriptName)
% '!':  ui.parseScript(ui,[scriptName])
%          if scriptName is not specified, the user is prompted to select a
%          script (a text file with extension .uisc, .txt, or .*), which is
%          then executed. See Scripting Syntax.
            
            if ~exist('scriptName','var') || ~exist(scriptName,'file')
                [scriptName, filepath] = uigetfile({'*.figui';'*.txt';'*.*'});
                scriptName = [filepath scriptName];
                clear filepath    
            end

            if scriptName == 0, ui.script = {}; return, end

            fid = fopen(scriptName,'r');
            ui.script = {};
            loop_list = 0;
            loop_skip = 0;
            loop_level = 1;

            while ~feof(fid)

                loop_list(1) = ftell(fid);
                f_cmd = strtrim(fgetl(fid));
                f_cmd = regexprep(f_cmd,'%.*$',''); % remove comments

                while ~isempty(f_cmd);              % multiple tokens per line
                    [n_cmd, f_cmd] = strtok(f_cmd); %#ok<STTOK>
                    n_cmd = strtrim(n_cmd);

                    if isempty(n_cmd), continue, end
                    
                    if loop_skip > 1
                        if strncmpi(n_cmd,'@{',2), loop_level = loop_level+1; end
                        if strncmpi(n_cmd,'@}',2), loop_level = loop_level-1; end            
                        if loop_level < loop_skip, loop_skip = 0; end    
                        continue
                    end       

                    if n_cmd(1) == '['  % Token is ARRAY literal
                        [n_cmd, f_cmd] = strtok([n_cmd f_cmd],']');
                        n_cmd = [n_cmd ']']; %#ok<*AGROW>
                        f_cmd = f_cmd(2:end);
                        ui.script{end+1} = evalin('base', n_cmd);

                    elseif n_cmd(1) == '{' % Token is CELL ARRAY literal
                        [n_cmd, f_cmd] = strtok([n_cmd f_cmd],'}');
                        n_cmd = [n_cmd '}']; %#ok<*AGROW>
                        f_cmd = f_cmd(2:end);
                        ui.script{end+1} = evalin('base', n_cmd);
                        
                    elseif n_cmd(1) == '"' % Token is STRING literal
                        [n_cmd, f_cmd] = strtok([n_cmd f_cmd],'"');
                        f_cmd = f_cmd(2:end);
                        ui.script{end+1} = n_cmd;

                    elseif n_cmd(1) == '''' % Token is STRING literal
                        [n_cmd, f_cmd] = strtok([n_cmd(2:end) f_cmd],'''');
                        ui.script{end+1} = n_cmd;
                        f_cmd = f_cmd(2:end);                        

                    elseif strncmpi(n_cmd,'@',1) % Compile-time command

                        if strncmpi(n_cmd,'@}',2) % Close-of-loop

                            evalin('base',f_cmd);
                            fseek(fid,loop_list(loop_level),-1);
                            loop_level = loop_level-1;

                        elseif strncmpi(n_cmd,'@{',2) % Start-of-loop

                            loop_level = loop_level+1;
                            loop_list(loop_level) = loop_list(1);                
                            if ~evalin('base',f_cmd), loop_skip = loop_level; end                

                        elseif strncmpi(n_cmd,'@~',2)
                            evalin('base',f_cmd);
                        else
                            ui.script{end+1} = evalin('base',f_cmd);                
                        end
                        f_cmd = [];
                    elseif ~isnan(str2double(n_cmd))
                        ui.script{end+1} = str2double(n_cmd);
                    else
                        ui.script{end+1} = n_cmd;
                    end
                end    
            end
            fclose(fid);


        end
        
        function ui = import(ui)
% '+':  ui.import(ui)
%         .import looks for a structure named 'UI_state' in the base
%          workspace. If it finds a structure named 'UI_state', then
%          ui.state = UI_state. If UI_state is not a scalar structure, the
%          user is asked to select which instance of UI_state to use.
        
            if ~evalin('base','exist(''UI_state'',''var'')')
                warning('UI_state is not a variable in the base workspace')
                return
            end
            WA = evalin('base','UI_state');   

            if ~isstruct(WA), 
                warning('UI_state is not a structure')
                return
            end

            % WA may not be scalar!
            while length(WA) > 1

                if ~isempty(ui.script)
                    i = ui.script{1}(1);
                    ui.script = ui.script(2:end);
                else

                    i = input(['Select UI_state [1..' ...
                                num2str(length(WA)) ']: ']);
                end
                if isempty(i), i = length(WA); end    
                i = min(length(WA),max(1,i));    
                disp(WA(i))

                confirm = input('Use this UI_state? [y/n]: ','s');
                if isempty(confirm) || strncmpi(confirm,'y',1) || strncmpi(confirm,'1',1)
                    WA = WA(i);
                end
                clear i confirm
            end

            ui.state = WA;
        end
        
        function ui = export(ui)
% '=':  ui.export(ui)
%         .export creates a copy of ui.state named 'UI_state' in the base
%          workspace. export and import can be used to save copies of your
%          UI state or to export data to other matlab applications.

        
            disp('UI state exported.')
            assignin('base','UI_state',ui.state);   
        end
                    
        function ui = stop(ui)
% 'Q':  ui.stop(ui)        
%         .stop causes the UI to exit from its main loop.
            ui.stopFlag = true;
        end

        function ui = parseClick(ui)
%  1 :  ui.parseClick(ui)
%         .parseClick retrieves the nearest text object to the point which 
%          the user clicked on the figure and calls the callback associated
%          with that string, if any. This can be used to build a menu which
%          can be used with either the mouse or keyboard. 
            
            [x,y] = ui.click;
            c = get(gca,'Children');
            
            b = '';
            dist = inf;
            
            
            for i = 1:length(c)
                
                if ~strcmpi(get(c(i),'type'),'text'), continue, end
                
                p = get(c(i),'position');
                d = (p(1) - x)^2 + (p(2) - y)^2;
                if d < dist, dist = d; b = get(c(i),'string'); end
            end
            
            if isempty(b), return, end
            b(1) = lower(b(1));
            
            if b(1) == '$' % execute in base

                if isempty(ui.script), 
                    evalin('base',input('>> ','s'));                    
                elseif strncmpi(b,'$~',2)
                    evalin('base',ui.script{1});
                    ui.script = ui.script(2:end);
                else
                    try
                        ui.script = [evalin('base',ui.script{1}) ui.script(2:end)];
                    catch                            
                        evalin('base',ui.script{1})
                    end
                end

            elseif b(1) == 27, ui.stopFlag = true;
            else
                i = find(ui.cmd == b(1), 1, 'last');
                if ~isempty(i)
                    try
                        ui = ui.callback{i}(ui); 
                    catch C
                        warning(C.getReport);
                    end
                end
            end            
        end
        
        function ui = editState(ui)
% 'O':  ui.editState(ui)
%         .editState is a function which allows a user or script to edit 
%          any of the fields of ui.state. Nested structures within ui.state
%          can be accessed recursively. This eliminates the need to write a
%          callback to edit every configuration variable; however, no
%          bounds checking is performed on the user-entered values.

            disp('Options: ')
            disp(ui.state)
            [ui,fn] = ui.input('opts.');
            if isfield(ui.state, fn) && isstruct(ui.state.(fn)),
                [ui, ui.state.(fn)] = setOptsStruct(ui.state.(fn),fn,ui);
                ui.state.update = 1;
                return
            elseif max(fn == '='),
                [fn, fv] = strtok(fn, '='); 
                fv = strtrim(fv(2:end));
                fn = strtrim(fn);
            else
                [ui,fv] = ui.input(['opts.' fn ' = ']);    
            end
            if ~isfield(ui.state, fn), 
                warning('figUI:editState:UnknownField',...
                       ['ui.editState: ' fn ' was not a field of ui.state (but it is now).']), 
                if ischar(fv)
                    ui.state.(fn) = eval(fv);
                else
                    ui.state.(fn) = fv;
                end
                return
            end
            if isstruct(ui.state.(fn)),
                [ui, ui.state.(fn)] = setOptsStruct(ui.state.(fn),fn,ui);
            elseif ischar(fv)
                ui.state.(fn) = eval(fv);
            else
                ui.state.(fn) = fv;
            end
            ui.state.update = 1;
        end

        function ui = pauseSuspend(ui)
% ' ':  ui.pauseSuspend(ui) (spacebar)
%         .pauseSuspend pauses the loop and returns. The loop may be
%          resumed at any point by clicking on the object associated with
%          ui.handles (typically a figure window, in which case you can
%          resume by clicking on the whitespace surrounding the axes).

            function pauseResume(~,~)
                set(ui.handles(1),'ButtonDownFcn',[])
                ui.loop;
            end
            
            set(ui.handles(1),'ButtonDownFcn',@(a,b)pauseResume(a,b))
            ui = ui.stop;
        end
        
        % Utility Functions
        
        function [ui, str] = input(ui, prompt)
%     [ui, str] = ui.input([prompt]);
%          If a script is being executed, it is echoed to the command line;
%          otherwise, the user is prompted to enter a string at the command
%          line as per str = input(prompt,'s');
            
            if ~exist('prompt','var'), prompt = ui.prompt; end
                        
            if ~isempty(ui.script)        
                str = ui.script{1};
                ui.script = ui.script(2:end);
                disp([prompt str])
            else                
                str = input(prompt,'s');
            end
        end
        
        function [ui,x,y,b] = ginput(ui)
%     [ui,(x,y,b)] = ui.ginput;
%          If a script is being executed, the next line of the script is 
%          interpreted as a user click. Otherwise, the user is prompted to 
%          select a point in the current axes as per [x,y,b] = ginput(1);

            fref = gcf;
            
            try
                if ~isempty(ui.script)
                    i = ui.script{1};            
                    if length(i) >= 3, x = i(1); y = i(2); b = i(3); %#ok<ALIGN>
                    elseif length(i) == 2 && ~ischar(i), x = i(1); y = i(2); b = 1;
                    else x = 0; y = 0; b = i; end

                    ui.script = ui.script(2:end);
                else
                    [x, y, b] = ginput(1);
                    
                    if isempty(x), 
                        x = ui.clickDat(1);
                        y = ui.clickDat(2);
                        b = 10;
                    end
                end
            catch C

                warning(C.message)
                if ~ishandle(fref), x = 0; y = 0; b = 27;
                else                x = 0; y = 0; b = 0; 
                end
            end
            
            ui.clickDat = [x,y,b];
        end
          
        function [x,y,b] = click(ui)
%     [x,y,b] = ui.click;
%         .click returns the coordinates and button for the most recently
%          clicked point on the current axes. .click can be used to
%          retreive the x and y coordinates of the keystroke used to enter
%          a callback.             
            x = ui.clickDat(1);
            y = ui.clickDat(2);
            b = ui.clickDat(3:end);
        end       
        
        function list = getBindings(ui)
%     [list] = ui.getBindings;
%         .getBindings returns a list of all of the currently assigned key
%          bindings, which is useful while developing calling .m files.
            
            list = cell(length(ui.cmd),2);
            
            for i = 1:length(ui.cmd)
                
                if ui.cmd(i) >= 'a' && ui.cmd(i) <= 'z'                
                     list{i,1} = upper(['' ui.cmd(i)]);
                else list{i,1} = ui.cmd(i);
                end
                list{i,2} = func2str(ui.callback{i});
            end
            
        end        
        
        function [ui,token] = getToken(ui)
%     [token] = ui.getToken;
%         .getToken returns the next script token if a script is currently 
%          being executed. This is useful for passing filenames to custom
%          load functions, for instance.
            if isempty(ui.script), token = '';
            else token = ui.script{1};
                 ui.script = ui.script(2:end);
            end
        end
    end
    
    methods (Hidden)
        
        function [ui,S] = editState_Struct(S,N,ui)
            disp([N ': '])
            disp(S)
            [ui,fn] = ui.input([N '.'], 's');
            if isfield(S, fn) && isstruct(S.(fn)),
                [ui, S.(fn)] = setOptsStruct(S.(fn),fn,ui);
            elseif max(fn == '='),
                [fn, fv] = strtok(fn, '='); 
                fv = strtrim(fv(2:end));
                fn = strtrim(fn);
            else
                [ui, fv] = ui.input([N '.' fn ' = ']);    
            end
            if ~isfield(S, fn), 
                warning('figUI:editState:UnknownField',...
                       ['ui.editState: ' fn ' was not a field of ''' N ''' (but it is now).']), 
                if ischar(fv)
                    S.(fn) = eval(fv);
                else
                    S.(fn) = fv;
                end
                return              
            end
            if isstruct(S.(fn)),
                [ui, S.(fn)] = setOptsStruct(S.(fn),fn,ui);
            elseif ischar(fv)
                S.(fn) = eval(fv);
            else
                S.(fn) = fv;
            end
        end
    end
    
end