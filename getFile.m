
function filename = getFile(filename, mode, varargin)

if nargin == 1, mode = 'newest'; end

if ~isempty(varargin), filename = sprintf(filename,varargin{:}); end

if strcmpi(mode,'next'), filename = get_next(filename);
  return
end


d = dir(filename); 

fp = fileparts(filename); 
if isempty(fp), fp = '.'; end
if isempty(d), error('Could not find a file "%s"', filename), end

switch lower(mode(1:3))
  case 'new', [~,sel] = max([d.datenum]);     
  case 'old', [~,sel] = min([d.datenum]); 
  case 'big', [~,sel] = max([d.bytes]); 
  case 'sma', [~,sel] = min([d.bytes]); 
  otherwise warning('getfile:unknownMode','unknown mode "%s"', mode)
    sel = 1;
end
  
filename = [fp filesep d(sel).name];  






function filename = get_next(filename)

[fp,fn,fe] = fileparts(filename); 
if isempty(fp), fp = '.'; end
fp = [fp filesep];

if contains(fn,'%t')
  fn = strrep(fn,'%t',datestr(now,'yyyymmdd@HHMMSS'));
  if isempty(dir([fp regexprep(fn,'@[\d]+([\)\]])?','$1') fe]))
       fn = regexprep(fn,'@[\d]+([\)\]])?','$1'); % remove (HHMMSS) if not needed
  else fn = regexprep(fn,'@(\d+)','-$1');
  end
elseif contains(fn,'%d')
  
  fn_star = regexprep(fn,'\s*\(?%d\)?','*'); 
  d = dir([fp fn_star fe]); 
  
  if isempty(d) % fn = regexprep(fn,'[\s]+[^\s]*%d[^\s]*\',''); 
      fn = strrep(fn,'%d',num2str(1));
  else
    fn_star = regexprep(fn,'.*(.)%d(.).*','''$1(\\d+)''$2');
    fn_star = regexprep(fn_star,'''([\[\(\)\]\\])','\\$1');    
    val = regexp({d.name},fn_star,'tokens','once'); 
    val = cellfun(@(v) str2double(v{1}),val(~cellfun(@isempty,val)));
    val = nanmax(val) + 1; 
    if isempty(val) || isnan(val), val = 1; end    
    fn = strrep(fn,'%d',num2str(val));
  end
elseif exist(filename,'file')
  warning('nextfile:overwrite','Preparing to overwrite %s',filename)
end


filename = [fp fn fe];

