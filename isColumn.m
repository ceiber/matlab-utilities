

function ok = isColumn(T,name)
ok = any(strcmp(T.Properties.VariableNames,name)); 
