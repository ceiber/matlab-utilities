function printInfo(varargin)

persistent s;
if nargin == 0, s = ''; return, end
fprintf('%s',char(8*ones(size(s)))); 
s = sprintf(varargin{:});
fprintf('%s',s)
end