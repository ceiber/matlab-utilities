
function renameLastFile(newname)

n = 1;
while exist(newname,'file')
    n = n+1;
    newname = regexprep(newname,'(\(\d+\))?\.',['(' num2str(n) ').']);
end

files = dir('*.mat');
[~,i] = max(cellfun(@datenum,{files.date}));
system(['ren "' files(i).name '" "' newname '"']);