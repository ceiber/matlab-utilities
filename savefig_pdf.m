
function savefig_pdf(name)

ext = '.svg'; 


if nargin == 0, name = ['fig' ext]; end
if ~strncmp(fliplr(name),fliplr(ext),4), name = [name ext]; end

if exist(name,'file')
  figs = dir(['fig*' ext]); 
  fnum = str2double(regexp({figs.name},'\d+','match','once')); 
  fnum = find(~ismember(1:numel(figs)+1,fnum),1);
  name = strrep(name,ext,sprintf('(%d)%s',fnum,ext));  
end

set(gcf,'PaperPositionMode','auto') %%#ok<UNRCH>
print(name,'-dsvg','-r0','-painters')

% % return
% system(['"C:\Program Files\gs\gs9.27\bin\gswin64c.exe" '          ...
%              '-sPAPERSIZE=archE -dEPSCrop -sDEVICE=pdfwrite '          ...
%              '-dNOPAUSE -dBATCH -dSAFER -dEncodeColorImages=false '   ...
%              '-dEncodeGrayImages=false -dEncodeMonoImages=false '     ...
%              '-sOutputFile="' name '" "' temp '"'])
% delete(temp)